import { createStore } from "pinia";
import { usePlayerStore } from "./playerStore";

interface Stat {
  number: number;
  initCost: number;
  increase: number;
  cost: number;
}

export interface Upgrade {
  name: string;
  bps: number;
  unlocksAt: number;
  disabled: boolean;
  quantity: Stat;
  level: Stat;
  placed: 0;
}

export const useUpgradesStore = createStore({
  id: "upgrades",
  state: (): Upgrade[] => require("@/data/upgrades.json"),
  getters: {
    getAvailableUpgrades(state) {
      const playerLevel = usePlayerStore().getLevel.value;
      return state
        .filter(upg => upg.unlocksAt - 1 <= playerLevel)
        .map(upg => {
          upg.unlocksAt - 1 === playerLevel
            ? (upg.disabled = true)
            : (upg.disabled = false);
          return upg;
        });
    }
  },
  actions: {
    buyUpgrade(index: number, quantity: number) {
      const upgrade = this.state[index];
      upgrade.quantity.number += quantity;
      upgrade.quantity.cost = Math.round(
        upgrade.quantity.initCost * upgrade.quantity.increase ** quantity
      );
    },
    levelUpgrade(index: number, quantity: number) {
      const upgrade = this.state[index];
      upgrade.level.number++;
      upgrade.level.cost = Math.round(
        upgrade.level.initCost * upgrade.level.increase ** quantity
      );
    },
    getPrice(data: Stat, quantity: number) {
      const initCost = data.initCost;
      const increase = data.increase;
      const initLevel = data.number;

      const range = (start: number, end: number): number[] => {
        return [...Array(end - start).keys()].map(v => start + v);
      };

      const sommeToN = (n: number): number => {
        if (n === 0) return 0;
        const allN = range(0, n); //index of upgrades from 0 to n-1
        const temp = allN.map(n => Math.round(initCost * increase ** n)); //array of costs from the first upgrade to the n-1 upgrade
        return temp.reduce((a, b) => a + b); //return the summary of all the cost
      };
      return sommeToN(initLevel + quantity) - sommeToN(initLevel);
    },
    getPriceBuy(index: number, quantity: number) {
      const upgrade = this.state[index];
      return this.getPrice(upgrade.quantity, quantity);
    },
    getPriceLevel(index: number, quantity: number) {
      const upgrade = this.state[index];
      return this.getPrice(upgrade.level, quantity);
    },
    resetUpgradeQuantity(index: number) {
      this.state[index].quantity.cost = this.state[index].quantity.initCost;
      this.state[index].quantity.number = 0;
    },
    resetUpgradeLevel(index: number) {
      this.state[index].level.cost = this.state[index].level.initCost;
      this.state[index].level.number = 0;
    },
    resetUpgrade(index: number) {
      this.resetUpgradeQuantity(index);
      this.resetUpgradeLevel(index);
    },
    resetAllUpgrades() {
      this.state.forEach((upgrade, index) => {
        this.resetUpgrade(index);
      });
    },
    placeUpgrade(index: number) {
      const upg = this.state[index];
      let res = false;
      if (upg.placed < upg.quantity.number) {
        upg.placed++;
        res = true;
      }
      return res;
    },
    removeUpgrade(index: number) {
      const upg = this.state[index];
      let res = false;
      if (upg.placed > 0) {
        upg.placed--;
        res = true;
      }
      return res;
    },
    getUpgradeIndexByName(name: string) {
      return this.state.findIndex(u => u.name === name);
    }
  }
});
