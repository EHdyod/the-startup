import { createStore } from "pinia";
import { Upgrade } from "@/store/upgradesStore";

export interface Room {
  contain: Upgrade | null;
}

export const useRoomStore = createStore({
  id: "Room",
  state: (): Room[] => [
    { contain: null },
    { contain: null },
    { contain: null },
    { contain: null },
    { contain: null },
    { contain: null }
  ],
  actions: {
    putUpgrade(upg: Upgrade, index: number) {
      this.state[index].contain = upg;
    },
    removeUpgrade(index: number) {
      this.state[index].contain = null;
    },
    getUpgradeInRoom(index: number): Upgrade | null {
      return this.state[index].contain;
    },
    getAllUpgradesInRoom() {
      return this.state.map(u => u.contain).filter(el => el !== null);
    },
    isIndexEmpty(index: number) {
      return this.state[index].contain === null;
    }
  }
});
