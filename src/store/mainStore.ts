import { createStore } from "pinia";
import { useUpgradesStore, Upgrade } from "./upgradesStore";
import { usePlayerStore } from "./playerStore";
import { useRoomStore } from "@/store/roomStore";

interface MainStore {
  bytes: number;
  totalBytes: number;
  bps: number;
  bpk: number;
  buyQuantity: number;
  levelQuantity: number;
}

export const useMainStore = createStore({
  id: "main",
  state: (): MainStore => ({
    bytes: 0,
    totalBytes: 0,
    bps: 0,
    bpk: 1,
    buyQuantity: 1,
    levelQuantity: 1
  }),
  getters: {
    getBytes(state) {
      return state.bytes;
    },
    getTotalBytes(state) {
      return state.totalBytes;
    },
    getBps(state) {
      return state.bps;
    },
    getBpk(state) {
      return state.bpk;
    },
    getBytesUntilNextLevel(state) {
      const player = usePlayerStore();
      return Math.round(player.getNextLevel.value - state.totalBytes);
    },
    getBuyQuantity(state) {
      return state.buyQuantity;
    },
    getLevelQuantity(state) {
      return state.levelQuantity;
    }
  },
  actions: {
    incrementBytes() {
      this.patch({
        bytes: this.state.bytes + this.state.bpk,
        totalBytes: this.state.totalBytes + this.state.bpk
      });
    },
    transaction(cost: number) {
      let res = false;
      if (this.state.bytes > cost) {
        this.patch({
          bytes: this.state.bytes - cost
        });
        res = true;
      }
      return res;
    },
    buyUpgrade(index: number) {
      const upgrades = useUpgradesStore();
      const quantity = this.state.buyQuantity;
      const costTotal = upgrades.getPriceBuy(index, quantity);
      if (this.transaction(costTotal)) upgrades.buyUpgrade(index, quantity);
    },
    levelUpgrade(index: number) {
      const upgrades = useUpgradesStore();
      const quantity = this.state.levelQuantity;
      const costTotal = upgrades.getPriceLevel(index, quantity);
      if (this.transaction(costTotal)) upgrades.levelUpgrade(index, quantity);
    },
    bytesPerSecond() {
      const upgrades = useRoomStore().getAllUpgradesInRoom();
      const bps = upgrades.reduce((acc: number, upg: Upgrade) => {
        return acc + upg.quantity.number * (upg.bps * upg.level.number);
      }, 0);
      this.patch({
        bps: bps,
        bytes: this.state.bytes + bps / 60,
        totalBytes: this.state.totalBytes + bps / 60
      });
    },
    changeBuyQuantity(quantity: number) {
      this.patch({
        buyQuantity: quantity
      });
    },
    changeLevelQuantity(quantity: number) {
      this.patch({
        levelQuantity: quantity
      });
    },
    placeUpgrade(indexUpg: number, indexRoom: number) {
      const upgrades = useUpgradesStore();
      const room = useRoomStore();
      if (upgrades.placeUpgrade(indexUpg)) {
        room.putUpgrade(upgrades.state[indexUpg], indexRoom);
      }
    },
    removeUpgrade(indexRoom: number) {
      const upgrades = useUpgradesStore();
      const room = useRoomStore();
      const upgrade = room.getUpgradeInRoom(indexRoom);
      if (upgrade !== null) {
        const indexUpg = upgrades.getUpgradeIndexByName(upgrade.name);
        if (upgrades.removeUpgrade(indexUpg)) {
          room.removeUpgrade(indexRoom);
        }
      }
    }
  }
});
