import { shallowMount } from "@vue/test-utils";
import Overview from "@/components/Overview.vue";

describe("Overview.vue", () => {
  const wrapper = shallowMount(Overview, {});

  it("should have 'overview-root' id.", () => {
    expect(wrapper.find("#overview-root").exists()).toBe(true);
  });
});
