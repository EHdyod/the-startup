import { shallowMount } from "@vue/test-utils";
import Upgrades from "@/components/Upgrades.vue";

describe("Upgrades.vue", () => {
  it("should have 'upgrades-root' id.", async () => {
    const wrapper = shallowMount(Upgrades);
    expect(wrapper.find("#upgrades-root").exists()).toBe(true);
  });
});
