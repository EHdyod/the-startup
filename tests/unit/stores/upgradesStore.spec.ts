import { useUpgradesStore } from "@/store/upgradesStore";

describe("upgrades store tests", () => {
  const store = useUpgradesStore();

  describe("states test", () => {
    const state = store.state;
    it("should state have upgrades", () => {
      expect(state).not.toBeNull();
    });
  });

  describe("getters tests", () => {
    it("should be only the first upgrade available at level 0 and the second upgrade should be disabled", () => {
      const upgrades = store.state;
      const upgradesAvailable = store.getAvailableUpgrades.value;
      expect(upgradesAvailable.length).toBe(2);
      expect(upgrades[0].disabled).not.toBe(true);
      expect(upgrades[1].disabled).toBe(true);
    });
  });

  describe("actions tests", () => {
    beforeEach(() => {
      store.resetAllUpgrades();
    });
    it("should quantity have increase by 5 after buy 5 upgrades", () => {
      const index = 0;
      const quantity = 5;
      store.buyUpgrade(index, quantity);
      const upgrade = store.state[index];
      expect(upgrade.quantity.number).toBe(quantity);
    });
    it("should be more expansive to buy 5 upgrades after buy 5 upgrades", () => {
      const index = 0;
      const quantity = 5;
      const initPrice = store.getPriceBuy(index, quantity);
      store.buyUpgrade(index, quantity);
      const finalPrice = store.getPriceBuy(index, quantity);
      expect(finalPrice > initPrice).toBe(true);
    });
    it("should return the price of 5 upgrades", () => {
      const index = 0;
      const quantity = 5;
      const priceRes = store.getPriceBuy(index, quantity);
      expect(priceRes).toBe(336);
    });
    it("should reset value of an update", () => {
      const index = 0;
      const initUpgrade = store.state[index];
      store.buyUpgrade(index, 5);
      store.resetUpgrade(index);
      const finalUpgrade = store.state[index];
      expect(finalUpgrade.quantity).toBe(initUpgrade.quantity);
    });
    it("should reset all value of updates", () => {
      const initUpgrade = store.state[0];
      store.buyUpgrade(0, 5);
      store.resetAllUpgrades();
      const finalUpgrade = store.state[0];
      expect(finalUpgrade.quantity).toBe(initUpgrade.quantity);
    });
  });
});
