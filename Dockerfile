FROM node:latest

RUN npm install -g http-server

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

ARG PORT

ENV PORT=${PORT}

EXPOSE ${PORT}
CMD ["http-server", "dist"]
